from sklearn.preprocessing import OneHotEncoder, LabelEncoder
import pandas as pd
import click


@click.argument()
@click.command("input_path", type=click.Path(exists=True))
@click.command("output_path", type=click.Path())
def cat_data_transform(input_path: str, output_path: str):
    """Function transforms categorical data in DataFrame
    by OneHotEncoder and LabelEncoder
    :param input_path: Path to original DataFrame
    :param output_path: Path to save transformed DataFrame
    :return:
    """
    df = pd.read_csv(input_path)

    oh_encoder = OneHotEncoder()
    l_encoder = LabelEncoder()

    l_data = df['model'].copy()
    l_data = l_encoder.fit_transform(l_data)

    oh_data = df[['transmission', 'fuel_type']].copy()
    oh_data = oh_encoder.fit_transform(oh_data)

    df.drop(['model', 'transmission', 'fuel_type'])
    df = pd.concat([l_data, df, oh_data])

    df.to_csv(output_path)


if __name__ == '__main__':
    cat_data_transform()
import pandas as pd
import click


@click.argument()
@click.command("input_path", type=click.Path(exists=True))
@click.command("output_path", type=click.Path())
def clean_data(input_path: str, output_path: str):
    """Function clear un correct features of DataFrame
    :param input_path: Path to original DataFrame
    :param output_path: Path to save clear DataFrame
    :return:
    """
    df = pd.read_csv(input_path)

    df = df.fillna(df.mean())

    df.to_csv(output_path)


if __name__ == '__main__':
    clean_data()

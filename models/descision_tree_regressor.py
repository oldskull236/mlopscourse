import pandas as pd
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import GridSearchCV
import pickle


def tree_regressor_best_params(X_train: pd.DataFrame,
                               y_train: pd.DataFrame,
                               ):
    """Function find and save model with best params
    """

    params = {
        'criterion': ['squared_error', 'absolute_error'],
        'max_depth': [3, 5, 10, None],
        'max_features': [1, 3, 5, 7],
        'min_samples_leaf': [1, 2, 3],
        'min_samples_split': [1, 2, 3]
    }

    tree_regressor = DecisionTreeRegressor()

    grid = GridSearchCV(tree_regressor, param_grid=params, cv=3)
    tree_grid = grid.fit(X_train, y_train)

    with open("../src/models/tree_params.pkl", 'wb') as file:
        pickle.dump(tree_grid, file)

    print('Model saved')
